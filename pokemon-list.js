Pokemon = require('./pokemon');

class PokemonList extends Array {

    constructor(...items){
       super(); // if you try to access this before super() is called, 
                // iojs will throw an error: ReferenceError: this is not defined. 
                // https://github.com/nodejs/node/issues/1723
       this.push(...items);     
    }

    add(name, level){
        this.push(new Pokemon(name, level));
    }
    show(caption){
        if(caption) console.log(`List <${caption}>`);
        this.forEach(e => e.show(), this);
        console.log(`Total size: ${this.length} pokemons`);
    }
    max(){
        let maxLvl = Math.max(...this);
        return this.find(e => e.valueOf() === maxLvl) || new Pokemon();
    }
    push(...items){
        items.forEach( e => {
            if(e instanceof Pokemon)
                super.push(e);
            if(Array.isArray(e))
                this.push(...e);
        });        
    }
    transfer(item, toList){     
        if(item instanceof Pokemon)
            return this.transfer(this.indexOf(item), toList);
        if(this[item])
            toList.push(this.splice(item,1));
    }
}

module.exports = PokemonList;
