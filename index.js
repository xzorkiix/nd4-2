var Pokemon = require('./pokemon'),
    PokemonList = require('./pokemon-list'),
    hidenseek = require('./hidenseek');

var pokemons = new PokemonList(require('./pokemons').map(obj => new Pokemon(obj.name, obj.level)));

const path = './field/';
hidenseek.hide(path, pokemons, (err, lost) => {
    if(err)
        console.error(err);
    else {
        lost.show('losted pokemons');
        hidenseek.seek(path, (err, find) =>  {
            if(err) console.log(err);
            find.show('and finded pokemons');
        });
    }
});

/* Пример вывода скрипта (если прежде не выполнялся, иначе находит больше покемонов чем теряем при выполнении)

List <losted pokemons>
pokemon Bulbasaur with 12 level
pokemon Charmander with 51 level
pokemon Venusaur with 22 level
Total size: 3 pokemons
List <and finded pokemons>
pokemon Charmander with 51 level
pokemon Venusaur with 22 level
pokemon Bulbasaur with 12 level
Total size: 3 pokemons

 */