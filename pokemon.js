class Pokemon {
    constructor(name, level){
        this.name = name;
        this.level = level;
    };
    valueOf(){
        return this.level;
    };
    show(){
        console.log(`pokemon ${this.name} with ${this.level} level`);
    }
}

module.exports = Pokemon;