var Pokemon = require('./pokemon'),
    PokemonList = require('./pokemon-list'),
    random = require('./random'),
    fs = require('fs'),
    path = require('path');

const pokemon_file_name = 'pokemon.txt';

function mkdir(dir) {
    if(!fs.existsSync(dir))
        fs.mkdirSync(dir);
}

exports.hide = (dir, pokemons, callback) => {
    
    let hided = new PokemonList();

    mkdir(dir);
    for(let i = 1; i <= 10; i++)
        mkdir(dir + '/' + ('0'+i).substr(-2) + '/');

    while(pokemons.length && hided.length < 3)
        pokemons.transfer(random(0,pokemons.length-1), hided);
    
    var saved = 0, errs = "";
    function async_done(err) {
        saved++;
        if(saved==hided.length)
            callback(errs, hided);
    }
    function saveAsync(pokemon) {
        let data = `${pokemon.name}|${pokemon.level}`;
        let filename = dir + '/' + ('0'+random(1,10)).substr(-2) + '/' + pokemon_file_name;
        if(fs.existsSync(filename)) 
            fs.appendFile(filename, '\n' + data, async_done);
        else
            fs.writeFile(filename, data, async_done);
    }    
    hided.forEach(e => saveAsync(e), this);
}

/** Функция должна «найти» в папке, указанной в первом аргументе, всех покемонов и вернуть PokemonList. */
exports.seek = (dir, callback) => {
    const find = new PokemonList();

    if(!fs.existsSync(dir)) {
        return callback({err: `Каталог ${dir} не существует`}, find);
    }
  
    // перед началом асинхроного метода включаем флаг, по окончанию снимаем (удаляем)
    // пустой массив - все асинхронные методы завершены
    const flags = []; 

    // ищем файлы только в катлогах 01/02/03 ...
    for(let i = 1; i <= 10; i++)
        readdir(dir + '/' + ('0'+i).substr(-2) + '/');

    function readdir(dir) {
        flags.push(dir);
        fs.readdir(dir, (err, files) => {
            if(Array.isArray(files))
            // если есть ошибка err - её обработает async_done
            if(!err)
                files.forEach(name => {                
                    filepath = path.join(dir, name);
                    let stat = fs.statSync(filepath);
                    if(stat.isFile()) {
                        if(name == pokemon_file_name)
                            parse(filepath);
                    } else
                        readdir(filepath);
                })
            async_done(err, dir);
        })
    }

    function parse(file){
        flags.push(filepath);
        fs.readFile(file, (err, data) => {
            if(data)
            data.toString().split('\n').map(str => {
                let arr = str.split('|');
                find.add(arr[0], arr[1]);
            });
            async_done(err, file);
        });
    }

    function async_done(err, filepath){
        flags.splice(flags.indexOf[filepath],1);
        if(flags.length == 0 || err)
            return callback(err, find);
    }
}